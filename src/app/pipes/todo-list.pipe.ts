import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'todoList'
})
export class TodoListPipe implements PipeTransform {

  transform(list: any[], status: string): any {

    if (!status) {
      return list;
    }
    if (status === 'all') {
      return list;
    } else if (status === 'active') {
      return list.filter(t => t.completed === true);
    } else if (status === 'completed') {
      return list.filter(t => t.completed === false);
    }
  }

}
