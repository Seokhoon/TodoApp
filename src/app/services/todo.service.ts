import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

export interface TodoItem {
  id: number;
  title: string;
  completed: boolean;
}

export class TodoItem implements TodoItem {
  constructor(public id: number, public title: string, public completed: boolean) {}
}


@Injectable()
export class TodoService {
  domain = 'http://localhost:8000';
  constructor(private http: Http) { }

  // Todo리스트 조회
  getAllTodos() {
    return this.http.get(this.domain + '/todos').map(res => res.json());
  }

  // todo 등록
  addTodo(title: string) {
    return this.http.post(this.domain + '/todo', {title: title}).map(res => res.json());
  }

  // todo 삭제
  removeTodo(id: number) {
    this.http.delete(this.domain + `/todo/${id}`).subscribe();
  }

  // 완료목록 삭제
  clearCompleted() {
    this.http.put(this.domain + '/todos/clearCompleted', {}).subscribe();
  }

  // todo 상태 변경
  toggleTodo(id: number) {
    this.http.put(this.domain + '/todos/toggle', {id: id}).subscribe();
  }

  // 전체 todo상태 변경
  toggleAllState(completed: boolean) {
    this.http.put(this.domain + '/todos/toggleAll', {status: completed}).subscribe();
  }

  // 전체 todo 완료 여부
  // toggleAllCompleted() {
  //   return this.http.put(this.domain + '/todos/toggleAllCompleted', {}).map(res => res.json());
  // }

}
