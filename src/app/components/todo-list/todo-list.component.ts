import { Component, OnInit } from '@angular/core';
import { TodoService, TodoItem } from '../../services/todo.service';
import { Observable } from 'rxjs/Observable';
import { TodoListPipe } from '../../pipes/todo-list.pipe';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styles: []
})
export class TodoListComponent implements OnInit {
  todos: TodoItem[];
  allCompleted = false;
  filter = 'all';

  constructor(private todoService: TodoService) {}

  ngOnInit() {
    this.todoService.getAllTodos().subscribe(data => {
      this.todos = data;
      this.checkAllCompleted();
    });
    // this.todoService.toggleAllCompleted().subscribe(data => this.allCompleted = data);
  }

  // todo조회
  getTodos() {
    if (this.filter === 'all') {
      return this.todos;
    } else if (this.filter === 'active') {
      return this.todos.filter(t => t.completed === false);
    } else if (this.filter === 'completed') {
      return this.todos.filter(t => t.completed === true);
    }
  }

  // 미완료 todo
  getLeftNum(): number {
    try {
      return this.todos.filter(t => t.completed === false).length;
    } catch (err) {
      return 0;
    }
  }

  // todo 등록
  addTodo(value: string) {
    this.todoService.addTodo(value).subscribe(data => {
      this.todos.push(new TodoItem(data, value, false));
      this.checkAllCompleted();
      }
    );
  }

  // todo 전체 완료 여부
  checkAllCompleted() {
    this.allCompleted = this.todos.length > 0 && this.todos.length === this.todos.filter(t => t.completed === true).length ? true : false;
  }

  // todo 전체 상태 변경
  toggleAllState(state: boolean) {
    this.todoService.toggleAllState(state);
    this.todos.map(todo => todo.completed = state);
    this.checkAllCompleted();
  }

  // todo 상태 변경
  toggleState(todo: TodoItem) {
    const todoItem = this.todos.find(t => t === todo);
    todoItem.completed = !todoItem.completed;
    this.todoService.toggleTodo(todo.id);
    this.checkAllCompleted();
  }

  // todo 삭제
  remove(id) {
    this.todoService.removeTodo(id);
    this.todos.splice(this.todos.findIndex(todo => todo.id === id), 1);
  }

  // 완료된 todo 삭제
  clearCompleted() {
    this.todoService.clearCompleted();
    this.todos = this.todos.filter(t => t.completed === false);
  }

  // todo 필터
  updateFilter(value) {
    this.filter = value;
  }

  enabledClearBtn() {
    return this.todos.filter(t => t.completed === true).length > 0;
  }


}
