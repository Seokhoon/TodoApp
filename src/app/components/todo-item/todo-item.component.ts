import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TodoService, TodoItem } from '../../services/todo.service';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html'
})
export class TodoItemComponent implements OnInit {
  @Input() todoItem: TodoItem;
  @Output() itemModified = new EventEmitter();
  @Output() itemRemoved = new EventEmitter();
  @Output() toggleStateEvent = new EventEmitter();

  constructor(private todoService: TodoService) {}

  ngOnInit() {
  }

  toggleState() {
    this.toggleStateEvent.next(this.todoItem);
  }

  updateItem() {
    this.itemModified.next(this.todoItem.id);
  }

  removeItem() {
    this.itemRemoved.next(this.todoItem.id);
  }
}
