import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-todo-header',
  templateUrl: './todo-header.component.html',
  styles: []
})
export class TodoHeaderComponent implements OnInit {
  @Output() addTodoEvent: EventEmitter<any> = new EventEmitter();
  @Output() toggleAllStateEvent: EventEmitter<any> = new EventEmitter();
  @Input() allCompleted: boolean;

  newTodo = '';

  constructor() {}

  ngOnInit() {
  }

  addTodo(value: string) {
    if (value.length > 0) {
      this.addTodoEvent.next(value);
      this.newTodo = '';
    }
  }

  toggleAllState(state: boolean) {
    this.toggleAllStateEvent.next(state);
  }

}
