import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-todo-app',
  templateUrl: './todo-app.component.html',
  providers: [TodoService]
})
export class TodoAppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
