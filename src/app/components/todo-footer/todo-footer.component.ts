import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { TodoItem } from '../../services/todo.service';

@Component({
  selector: 'app-todo-footer',
  templateUrl: './todo-footer.component.html',
  styles: []
})
export class TodoFooterComponent implements OnInit {
  // @Input() todos: TodoItem[];
  @Input() leftNum: number;
  @Input() enabledClearBtn: boolean;
  @Output() filterEvent = new EventEmitter();
  @Output() clearCompletedEvent = new EventEmitter();
  status = 'all';

  constructor() {}

  ngOnInit() {
  }

  getAll() {
    this.status = 'all';
    // this.setFilter({filter : this.status});
    this.setFilter(this.status);
  }

  getActive() {
    this.status = 'active';
    this.setFilter(this.status);
  }

  getCompleted() {
    this.status = 'completed';
    this.setFilter(this.status);
  }

  clearCompleted() {
    this.clearCompletedEvent.next();
  }

  setFilter(filterObj) {
    this.filterEvent.next(filterObj);
  }
}
